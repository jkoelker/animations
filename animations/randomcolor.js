/* Copyright (c) 2018 ColorBit LLC
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
var colors = require('colors.js');

module.exports = {
  init() {
    state.init = false;
    state.color = { r: 0, g: 0, b: 0 };
  },

  anim() {
    if (!state.init) {
      state.color = colors.random();
      LEDS[0] = state.color;
      mirror(LEDS, 1);
      show(LEDS);
      state.init = true;
    }
  },

  params: {
  },

  name: 'Random Color',
  description: 'A single random color fills the string',
};
