/* Copyright (c) 2018 ColorBit LLC
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
var colors = require('colors.js');
var types = require('types.js');
var descriptions = require('desc.js');

module.exports = {
  init() {
    state.up = true;
    state.led = 0;
    state.width = LEDS.length / params.Number_Of_Chasers;
    state.color = params.Color[0];
    state.currentColor = 0;
    state.lastColor = params.Color.length;
    state.changeColor = false;
  },

  anim() {
    LEDS[state.led] = state.color;
    mirror(LEDS, state.width);
    show(LEDS);
    fade_all_to_black(LEDS, params.Fade_Speed);
    delay(params.Animation_Speed);

    if (state.up) {
      state.led += 1;
    } else {
      state.led -= 1;
    }

    if (params.Wrap_Around) {
      if (state.led >= state.width) {
        state.led = 0;
        state.changeColor = true;
      }
    } else if (state.led >= state.width) {
      state.up = false;
      state.led = state.width - 1;
      state.changeColor = true;
    } else if (state.led <= 0) {
      state.up = true;
      state.led = 0;
      state.changeColor = true;
    }

    // change color
    if (state.changeColor) {
      if (params.Random_Color) {
        state.color = colors.random();
      } else if (state.currentColor === state.lastColor - 1) {
        state.color = params.Color[0];
        state.currentColor = 0;
      } else {
        state.currentColor += 1;
        state.color = params.Color[state.currentColor];
      }
      state.changeColor = false;
    }
  },

  params: {
    Color: types.array(
      [
        colors.Red,
        colors.Blue,
        colors.Green,
      ],
      types.rgb(),
      descriptions.Colors
    ),
    Number_Of_Chasers: types.number(1, descriptions.NumberOfAnimations),
    Animation_Speed: types.number(20, descriptions.AnimationSpeed),
    Fade_Speed: types.number(15, descriptions.FadeToBlackSpeed),
    Wrap_Around: types.bool(false, descriptions.WrapAround),
    Random_Color: types.bool(false, descriptions.RandomAnimationColors),
  },

  name: 'Chase',
  description: 'Chasing colors with a tail',
};
