/* Copyright (c) 2018 ColorBit LLC
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
var colors = require('colors.js');
var types = require('types.js');
var descriptions = require('desc.js');

module.exports = {
  init() {
    state.up = true;
    state.led = 0;
    state.width = LEDS.length / params.Number_Of_Chasers;
    state.colors = [];
    if (params.Random_Color) {
      state.colors[0] = rgb2hsv(colors.random());
      state.colors[1] = rgb2hsv(colors.random());
    } else {
      params.Color.forEach(function hsv(c) {
        state.colors.push(rgb2hsv(c));
      });
    }
    state.current = 0;
    state.target = 1;
    state.step = 0;
    state.donefading = 127;
  },

  anim() {
    // change color
    var speed = lib8tion.cubicwave8(state.step);
    var current = state.colors[state.current];
    var target = state.colors[state.target];
    var color = blend_hsv(current, target, speed);

    LEDS[state.led] = hsv2rgb(color);
    mirror(LEDS, state.width);
    show(LEDS);
    fade_all_to_black(LEDS, params.Fade_Speed);
    delay(params.Animation_Speed);

    if (state.up) {
      state.led += 1;
    } else {
      state.led -= 1;
    }

    if (params.Wrap_Around) {
      if (state.led >= state.width) {
        state.led = 0;
      }
    } else if (state.led >= state.width) {
      state.up = false;
      state.led = state.width - 1;
    } else if (state.led <= 0) {
      state.up = true;
      state.led = 0;
    }

    state.step += params.Color_Blend_Speed;
    if (state.step > state.donefading) {
      state.step = 0;

      if (params.Random_Color) {
        state.colors[0] = state.colors[1];
        state.colors[1] = rgb2hsv(colors.random());
      } else {
        state.current += 1;
        state.target += 1;

        if (state.current >= state.colors.length) {
          state.current = 0;
        }

        if (state.target >= state.colors.length) {
          state.target = 0;
        }
      }
    }
  },

  params: {
    Color: types.array(
      [
        colors.Red,
        colors.Blue,
        colors.Green,
      ],
      types.rgb(),
      descriptions.Colors
    ),
    Number_Of_Chasers: types.number(1, descriptions.NumberOfAnimations),
    Animation_Speed: types.number(20, descriptions.AnimationSpeed),
    Color_Blend_Speed: types.number(4, descriptions.BlendSpeed),
    Fade_Speed: types.number(10, descriptions.FadeToBlackSpeed),
    Wrap_Around: types.bool(false, descriptions.WrapAround),
    Random_Color: types.bool(false, descriptions.RandomAnimationColors),
  },

  name: 'Chase (Blending Colors)',
  description: 'Chasing colors with a tail',
};
